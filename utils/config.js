import hex_md5 from './md5.js';
import toutf from './base64.js';
let env = 'prd';
let thirdUrl, requestUrl, key;
if (env == 'prd'){
  thirdUrl = 'https://api.winhc.cn/miniapp/'; 
  requestUrl = 'https://api.winhc.cn/namecard/';
  key = 'iroolf958i09d0lg69fg';
}else{
  thirdUrl = 'https://t.winhc.net/miniapp/';//http://106.14.81.247:9094/
  requestUrl = 'https://t.winhc.net/namecard/';//http://10.1.10.142:8792/
  key = '123456';
}

const appid = "wxb70cce7f9a8afb10";

const signType = "MD5";
const request_api = (res,fnScu,fnFail) =>{
  wx.request({
    url: thirdUrl +'sessionKey', //仅为示例，并非真实的接口地址
    method: 'GET',
    data: {
      code: res
    },
    header: {
      'content-type': 'application/json' // 默认值
    },
    success: function (obj) {
      fnScu(obj);
    },
    fail: function (obj) {
      fnFail(obj);
    }
  })
}

const requestGetInfo = (serviceName, bodyInfo, fnScu, fnFail) =>{
  let signInfo = hex_md5(toutf._utf8_encode(JSON.stringify(bodyInfo) + key));
  wx.request({
    url: requestUrl + serviceName, //仅为示例，并非真实的接口地址
    method: 'GET',
    data: {
      "serviceName": serviceName,
      "signType": signType,
      "sign": signInfo,
      "body": bodyInfo
    },
    header: {
      'content-type': 'application/json' // 默认值
    },
    success: function (obj) {
      fnScu(obj);
    },
    fail: function (obj) {
      fnFail(obj);
    }
  })
}

const requestPostInfo = (serviceName, bodyInfo, fnScu, fnFail) => {
  let signInfo = hex_md5(toutf._utf8_encode(JSON.stringify(bodyInfo) + key));
  wx.request({
    url: requestUrl + serviceName, //仅为示例，并非真实的接口地址
    method: 'POST',
    data: {
      "serviceName": serviceName,
      "signType": signType,
      "sign": signInfo,
      "body": JSON.stringify(bodyInfo)
    },
    header: {
      'content-type': 'application/x-www-form-urlencoded' // 默认值
    },
    success: function (obj) {
      fnScu(obj);
    },
    fail: function (obj) {
      fnFail(obj);
    }
  })
}

const requestUploadImg = (serviceName, bodyInfo, fnScu, fnFail) => {
  let signInfo = hex_md5(toutf._utf8_encode(JSON.stringify(bodyInfo) + key));
  wx.uploadFile({
    url: requestUrl + serviceName, //仅为示例，非真实的接口地址
    filePath: bodyInfo.path,
    name: 'file',
    formData: {
      sign: signInfo,
      body: JSON.stringify(bodyInfo)
    },
    success: function (obj) {
      fnScu(obj);
    },
    fail: function (obj) {
      fnFail(obj);
    }
  })
}


module.exports = {
  request_api: request_api,
  get_api: requestGetInfo,
  post_api: requestPostInfo,
  requestUploadImg:requestUploadImg,
  request_url:thirdUrl
};
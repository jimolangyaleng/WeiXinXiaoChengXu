// pages/collect/collect.js
import formatDate from '../../utils/util.js';
import request from '../../utils/config.js';
const app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    ossImgUrL: 'https://winhc.oss-cn-shanghai.aliyuncs.com/xcx/',
    selectShow: true,
    whoCollectTome: true,
    bussinessInfoList: new Array()
  },

  // 按时间进行排序
  sortByTime: function (tempList, _that) {
    let filterArray = formatDate.sortByTime(tempList);
    filterArray.forEach(function (e) {
      e.initial = formatDate.formatDate(e.initial);
    });
    _that.setData({
      bussinessInfoList: filterArray
    });
  },

  //谁收藏了我
  whoCollect: function () {
    wx.showLoading({
      title: '加载中',
    })
    let bodyInfo = {
      pageNum: "1",
      pageSize: "100",
      openid: app.globalData.openid
    };
    let _that = this;
    request.get_api("otherFavorUser", bodyInfo, (data) => {
      console.log("谁收藏了我列表成功", data)
      let bodyInfo = JSON.parse(data.data.body);
      // 判断对方是否注册
      let newOther = new Array();
      for (let i = 0; i < bodyInfo.length; i++) {
        // console.log(bodyInfo[i])
        if (bodyInfo[i].lawfirmName == null) {
          bodyInfo[i].lawfirmName = '暂无'
          bodyInfo[i].openid = null;
          newOther.push(bodyInfo[i])
        } else {
          newOther.push(bodyInfo[i])
        }
      }
      _that.sortByTime(newOther, _that)

      wx.hideLoading()
    }, (data) => {
      wx.hideLoading()
      console.log("谁收藏了我列表失败", data)
    });
    this.setData({
      selectShow: true
    })
  },

  //我收藏了谁
  iCollect: function () {
    wx.showLoading({
      title: '加载中',
    })
    let bodyInfo = {
      pageNum: "1",
      pageSize: "100",
      openid: app.globalData.openid
    };
    let _that = this;
    request.get_api("userFavorOther", bodyInfo, (data) => {
      console.log("我收藏了谁列表成功", data)
      let bodyInfo = JSON.parse(data.data.body);
      let newOther2 = new Array();
      for (let i = 0; i < bodyInfo.length; i++) {
        // console.log(bodyInfo[i])
        if (bodyInfo[i].userName != null) {
          newOther2.push(bodyInfo[i])
        }
      }
      _that.sortByTime(newOther2, _that)
      wx.hideLoading()
    }, (data) => {
      wx.hideLoading()
      console.log("我收藏了谁列表失败", data)
    });
    this.setData({
      selectShow: false
    })
  },

  //点击进入浏览用户信息页面
  goSeeOtherIndex: function (e) {
    let openid = e.currentTarget.dataset.openid;
    if (openid != null) {
      wx.navigateTo({
        url: '../otherIndex/otherIndex?openid=' + openid
      })
    } else {
      wx.showToast({
        title: '对方未创建名片',
        icon: 'loading',
        duration: 1000
      })
    }
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.whoCollect();
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    wx.stopPullDownRefresh();
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  }

})
// pages/photoalbum/photoalbum.js
const app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    indicatorDots: true,
    autoplay: false,
    bgImage: "",
    userName: "",
    time:"",
    current: "0",
    currentPage: '0',
    indicatorActiveColor: '#707070',
    showImgList:new Array(),
    interval: 5000,
    duration: 1000
  },


  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var indexId = options.key;
    //获取存储的图片列表
    let showMoreImgData = wx.getStorageSync("showMoreImgList");
    // console.log('获取图片信息', showMoreImgData);
    if (showMoreImgData){
      let startTime = showMoreImgData.time;
      let time = startTime.substring(0, 4) + "-" + startTime.substring(4, 6) + "-" + startTime.substring(6, 8) + " " + startTime.substring(8, 10) + ":" + startTime.substring(10, 12) + ":" + startTime.substring(12, 14);
      this.setData({
        current: indexId,
        bgImage: showMoreImgData.avatarUrl,
        userName: showMoreImgData.userName,
        time:time,
        showImgList: showMoreImgData.images
      });
    }
  },

  //显示相册
  showImageList: function(e){
    console.log("相册预览照片", e);
    var tempUrl = e.currentTarget.dataset.url;
    var imgList = this.data.showImgList;
    wx.previewImage({
      current: tempUrl, // 当前显示图片的http链接
      urls: imgList // 需要预览的图片http链接列表
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    wx.showShareMenu({
      withShareTicket: true
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  //swiper触发改变
  changeSwiper: function(e){
    console.log("触发swiper改变=",e)
    this.setData({
      currentPage: e.detail.current
    });
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function (res) {
    if (res.from === 'button') {
      // 来自页面内转发按钮
      console.log("来自页面内的转发", res.target)
    }
    return {
      title: '您好，这是我的关联相册',
      path: 'pages/photoalbum/photoalbum?key=' + this.data.key,
      // imageUrl: picUrl,
      success: function (res) {
        // 转发成功
        console.log("转发成功返回数据=", res)
        wx.showShareMenu({
          // 要求小程序返回分享目标信息
          withShareTicket: true
        });
      },
      fail: function (res) {
        // 转发失败
      },
      complete: function () {

      }
    }
  }
})
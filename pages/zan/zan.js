// pages/moods/moods.js
import formatDate from '../../utils/util.js';
import request from '../../utils/config.js';
const app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    ossImgUrL: 'https://winhc.oss-cn-shanghai.aliyuncs.com/xcx/',
    selectShow: true,
    bussinessInfoList: new Array()
  },

  // 按时间进行排序
  sortByTime: function (tempList, _that) {
    let filterArray = formatDate.sortByTime(tempList);
    filterArray.forEach(function (e) {
      e.initial = formatDate.formatDate(e.initial);
    });
    _that.setData({
      bussinessInfoList: filterArray
    });
  },

  //谁赞过我
  whoZan: function () {
    wx.showLoading({
      title: '加载中',
    })
    let bodyInfo = {
      pageNum: "1",
      pageSize: "100",
      openid: app.globalData.openid
    };
    let _that = this;
    request.get_api("whoLikeMe", bodyInfo, (data) => {
      console.log("谁赞过我列表成功", data)
      let bodyInfo = JSON.parse(data.data.body);

      // 判断对方是否注册
      let newOther = new Array();
      for (let i = 0; i < bodyInfo.length; i++) {
        console.log(bodyInfo[i])
        if (bodyInfo[i].lawfirmName == null) {
          bodyInfo[i].lawfirmName = '暂无'
          bodyInfo[i].openid = null;
          newOther.push(bodyInfo[i])
        } else {
          newOther.push(bodyInfo[i])
        }
      }
      _that.sortByTime(newOther, _that)

      // _that.sortByTime(bodyInfo, _that)
      wx.hideLoading()
    }, (data) => {
      wx.hideLoading()
      console.log("谁赞过我列表失败", data)
    });
    this.setData({
      selectShow: true
    })
  },

  //我赞过谁
  iZan: function () {
    wx.showLoading({
      title: '加载中',
    })
    let bodyInfo = {
      pageNum: "1",
      pageSize: "100",
      openid: app.globalData.openid
    };
    let _that = this;
    request.get_api("meLikeWho", bodyInfo, (data) => {
      console.log("我赞过谁列表成功", data)
      let bodyInfo = JSON.parse(data.data.body);
      _that.sortByTime(bodyInfo, _that)
      wx.hideLoading()
    }, (data) => {
      wx.hideLoading()
      console.log("我赞过列表失败", data)
    });
    this.setData({
      selectShow: false
    })
  },

  //点击进入浏览用户信息页面
  goSeeOtherIndex: function (e) {
    let openid = e.currentTarget.dataset.openid;
    if (openid != null) {
      wx.navigateTo({
        url: '../otherIndex/otherIndex?openid=' + openid
      })
    } else {
      wx.showToast({
        title: '对方未创建名片',
        icon: 'loading',
        duration: 1000
      })
    }
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.whoZan();
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    wx.stopPullDownRefresh();
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})
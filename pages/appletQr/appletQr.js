// pages/appletQr/appletQr.js
//获取应用实例
const app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    qrData: "",
    userName: "",
    nickUrl: ""
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    wx.showLoading({
      title: '加载中',
    })
    let transData = JSON.parse(options.transData);
    let _this = this;
    wx.downloadFile({
      url: transData.avatarUrl,
      success: function (res) {
        _this.setData({
          userName: transData.name,
          nickUrl: res.tempFilePath
        });
      },
      fail: function (res) {
        console.log("下载失败=", res)
      }
    })

  },

  //长按事件
  longTap: function () {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成 
   */
  onReady: function () {
    console.log("wx.getSystemInfoSync()=", wx.getSystemInfoSync())
    //获取手机屏幕的宽高
    let windowWidth = wx.getSystemInfoSync().windowWidth;
    let windowHeight = wx.getSystemInfoSync().windowHeight;
    if (app.globalData.qrData) {
      let _this = this;
      wx.downloadFile({
        url: app.globalData.qrData,
        success: function (res) {
          console.log(res);
          const ctx = wx.createCanvasContext('myQrCanvas');
          var nickUrl = _this.data.nickUrl;
          var tipMsg = _this.data.userName;

          ctx.setFillStyle("#1b82d1");
          ctx.fillRect(0, 0, windowWidth, windowHeight)

          let circle = {
            x: windowWidth / 2,    // 圆心的x轴坐标值
            y: -330,    // 圆心的y轴坐标值
            r: 492     // 圆的半径
          };
          ctx.setFillStyle("#ffffff");
          ctx.arc(circle.x, circle.y, circle.r, 0, Math.PI * 2, false);
          ctx.fill();

          ctx.save(); // 保存当前ctx的状态
          ctx.beginPath();
          ctx.arc(windowWidth / 2, 50, 30, 0, Math.PI * 2, false); //画出圆
          ctx.setStrokeStyle("#d3d3d3");
          ctx.stroke();
          ctx.clip(); //裁剪上面的圆形
          ctx.drawImage(nickUrl, (windowWidth - 60) / 2, 20, 60, 60); //头像
          ctx.restore(); // 还原状态

          ctx.setFontSize(16);
          ctx.setTextAlign('right');
          ctx.setFillStyle("#1b82d1");
          ctx.fillText(tipMsg, windowWidth / 2 - 23, 110);
          ctx.setTextAlign('left');
          ctx.setFillStyle("black");
          ctx.fillText("专属小程序码", windowWidth / 2 - 20, 110);
          ctx.setFontSize(12);
          ctx.setFillStyle("#666666");
          ctx.setTextAlign('center');
          ctx.fillText('长按或扫描进入', windowWidth / 2, 130);


          ctx.setFillStyle("#ffffff");
          ctx.fillText(_this.data.userName + "名片码", windowWidth / 2, 290);

          ctx.setFontSize(12);
          ctx.drawImage('../../img/logo.png', (windowWidth - 20) / 2, windowHeight - 75, 20, 20);
          ctx.setFillStyle("#ffffff");
          ctx.fillText('赢火虫名片', windowWidth / 2, windowHeight - 35);

          ctx.setFontSize(9);
          ctx.setFillStyle("#ffffff");
          ctx.fillText('—— 创建赢火虫名片，获得专属小程序码 ——', windowWidth / 2, windowHeight - 20);

          ctx.setFontSize(12);
          ctx.save();
          ctx.beginPath();
          ctx.arc(windowWidth / 2, 210, 60, 0, Math.PI * 2, false); //画出圆
          ctx.setFillStyle("#ffffff");
          ctx.fill();
          ctx.clip(); // 裁剪上面的圆形
          ctx.drawImage(res.tempFilePath, (windowWidth - 100) / 2, 160, 100, 100);
          ctx.restore(); // 还原状态

          ctx.draw();
          setTimeout(function () {
            wx.canvasToTempFilePath({
              x: 0,
              y: 0,
              // width: windowWidth,
              // height: windowHeight,
              // destWidth: windowWidth,
              // destHeight: windowHeight,
              quality: 1,
              canvasId: 'myQrCanvas',
              success: function (res) {
                console.log("导出成功临时路径tempFilePath=", res.tempFilePath)
                wx.previewImage({
                  current: res.tempFilePath, // 当前显示图片的http链接
                  urls: [res.tempFilePath] // 需要预览的图片http链接列表
                });
                wx.hideLoading();
              },
              fail: function (res) {
                console.log("导出失败临时路径tempFilePath=", res.tempFilePath)
              }
            })
          }, 100);
        },
        fail: function (res) {
          console.log("下载失败=", res)
        }
      })
    }
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    wx.stopPullDownRefresh();
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  }

  /**
   * 用户点击右上角分享
   */
  // onShareAppMessage: function () {

  // }
})
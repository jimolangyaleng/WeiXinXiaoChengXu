// pages/qRcode/qRcode.js
const app = getApp();
Page({
  /**
   * 页面的初始数据
   */
  data: {
    userData: "",
    nickUrl:"",
    qrUrl: '',
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    console.log("options=", options)
    let userData = JSON.parse(options.infoData);;
    this.setData({
      userData: userData
    });

    wx.setNavigationBarTitle({
      title: this.data.userData.userName + '的名片二维码',
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    wx.showLoading({
      title: '加载中',
    })
    //下载图片
    let userData = this.data.userData;
    let _this = this;
    wx.downloadFile({
      url: userData.avatarUrl,
      success: function (res) {
        _this.setData({
          nickUrl: res.tempFilePath
        });
        wx.downloadFile({
          url: app.globalData.goQrcodeUrl,
          success: function (res) {
            _this.setData({
              qrUrl: res.tempFilePath
            });
            let windowWidth = wx.getSystemInfoSync().windowWidth;
            let windowHeight = wx.getSystemInfoSync().windowHeight;
            const ctx = wx.createCanvasContext('myQrCodeCanvas');
            ctx.setFillStyle("#f0eff4")
            ctx.fillRect(0, 0, windowWidth, windowHeight)

            // ctx.setStrokeStyle('#908f8f')
            // ctx.strokeRect(10, 10, windowWidth - 20, windowHeight - 20); 

            ctx.setFillStyle('#1b82d1')
            ctx.fillRect(0, 0, windowWidth, windowHeight*0.23);
            ctx.drawImage('../../img/logo2.png', windowWidth / 2 - windowWidth * 0.075, windowHeight * 0.03, windowWidth*0.15 ,windowWidth*0.15);
            
            ctx.setFillStyle('#ffffff')
            ctx.setFontSize(21);
            ctx.setTextAlign('center')
            ctx.fillText("赢火虫名片", windowWidth / 2 , windowHeight*0.17);

            ctx.setFillStyle('#ffffff')
            ctx.setFontSize(12);
            ctx.setTextAlign('center')
            ctx.fillText("您好这是我的名片，望惠存", windowWidth / 2, windowHeight * 0.207);

            ctx.setFillStyle('#ffffff')
            ctx.fillRect(10, windowHeight * 0.245, windowWidth - 20, windowHeight * 0.236); 

            // 绘制圆角矩形 ———————— start ——————
           
            // 绘制圆角矩形 ————————  end  ——————
            ctx.setTextAlign('left')
            ctx.setFillStyle('#010101')
            ctx.setFontSize(18);
            ctx.fillText(_this.data.userData.userName, windowWidth * 0.07, windowHeight * 0.32);
            ctx.setFillStyle('#333333')
            ctx.setFontSize(11);
            ctx.fillText(_this.data.userData.title, windowWidth * 0.3, windowHeight * 0.32);

            ctx.save(); // 保存当前ctx的状态
            ctx.beginPath();
            ctx.arc((windowWidth - 60), 190, 30, 0, Math.PI * 2, false); //画出圆
            ctx.setStrokeStyle("#d3d3d3");
            ctx.stroke();
            ctx.clip(); // 裁剪上面的圆形
            ctx.drawImage(_this.data.nickUrl, (windowWidth - 90), 160, 60, 60);
            ctx.restore(); // 还原状态

            ctx.beginPath();
            ctx.setFillStyle('#1b82d1')
            ctx.setFontSize(10);
            ctx.fillText("手机", windowWidth * 0.07, windowHeight * 0.38);
            ctx.setFillStyle('#1b82d1')
            ctx.setFontSize(18);
            ctx.fillText(_this.data.userData.mobileNo, windowWidth * 0.2, windowHeight * 0.38);

            ctx.beginPath();
            ctx.setFillStyle('#1b82d1')
            ctx.setFontSize(10);
            ctx.fillText("公司", windowWidth * 0.07, windowHeight * 0.416);
            ctx.setFillStyle('#000000')
            ctx.setFontSize(14);
            ctx.fillText(_this.data.userData.lawfirmName, windowWidth * 0.2, windowHeight * 0.416);

            ctx.beginPath();
            ctx.setFillStyle('#1b82d1')
            ctx.setFontSize(10);
            ctx.fillText("Email", windowWidth * 0.07, windowHeight * 0.452);
            ctx.setFillStyle('#000000')
            ctx.setFontSize(12);
            ctx.fillText(_this.data.userData.email, windowWidth * 0.2, windowHeight * 0.452);

            ctx.setFillStyle('#ffffff')
            ctx.fillRect(10, windowHeight * 0.496, windowWidth - 20, windowHeight * 0.127); 

            ctx.drawImage('../../img/renqi_slt.png', (windowWidth - 20) * 1 / 6, windowHeight * 0.52, 20, 18);
            ctx.setFillStyle('#000000')
            ctx.setFontSize(12)
            ctx.setTextAlign('center')
            ctx.fillText('人气：' + _this.data.userData.seeNum, (windowWidth - 20) * 1 / 6+10, windowHeight * 0.6)

            if (_this.data.userData.voteNum > 0) {
              ctx.drawImage('../../img/zan_slt.png', (windowWidth - 20) * 1 / 2, windowHeight * 0.52, 20, 18);
            } else if (_this.data.userData.voteNum == 0) {
              ctx.drawImage('../../img/zan.png', (windowWidth - 20) * 1 / 2, windowHeight * 0.52, 20, 18);
            }
            ctx.setFillStyle('#000000')
            ctx.setFontSize(12)
            ctx.setTextAlign('center')
            ctx.fillText('赞：' + _this.data.userData.voteNum, (windowWidth - 20) * 1 / 2+10, windowHeight * 0.6)

            if (_this.data.userData.msgNum>0) {
              ctx.drawImage('../../img/liuyan_slt.png', (windowWidth - 20) * 5 / 6, windowHeight * 0.52, 15, 18);
            } else if (_this.data.userData.msgNum == 0) {
              ctx.drawImage('../../img/liuyan.png', (windowWidth - 20) * 5 / 6, windowHeight * 0.52, 15, 18);
            }
            ctx.setFillStyle('#000000')
            ctx.setFontSize(12)
            ctx.setTextAlign('center')
            ctx.fillText('留言：' + _this.data.userData.msgNum, (windowWidth - 20) * 5 / 6+10, windowHeight * 0.6)


            ctx.setFillStyle('#ffffff')
            ctx.fillRect(10, windowHeight * 0.638, windowWidth - 20, windowHeight * 0.23);

            ctx.drawImage(res.tempFilePath, 30, windowHeight * 0.67, 100, 100);

            ctx.setFillStyle('#888888')
            ctx.setFontSize(12)
            ctx.setTextAlign('left')
            ctx.fillText('长按或扫一扫识别', (windowWidth - 50) * 1 / 2, windowHeight * 0.69)

            ctx.setFillStyle('#000000')
            ctx.setFontSize(14)
            ctx.setTextAlign('left')
            ctx.fillText('同步名片至通讯录', (windowWidth - 50) * 1 / 2, windowHeight * 0.725)

            ctx.beginPath()
            ctx.setLineWidth(1)
            ctx.setStrokeStyle('#f0eff4')
            ctx.moveTo((windowWidth - 50) * 1 / 2, windowHeight * 0.745)
            ctx.lineTo((windowWidth - 50), windowHeight * 0.745)
            ctx.stroke()

            ctx.setFillStyle('#888888')
            ctx.setFontSize(12)
            ctx.setTextAlign('left')
            ctx.fillText('发现-小程序-搜索', (windowWidth - 50) * 1 / 2, windowHeight * 0.78)

            ctx.setFillStyle('#000000')
            ctx.setFontSize(14)
            ctx.setTextAlign('left')
            ctx.fillText('\"赢火虫名片\"', (windowWidth - 50) * 1 / 2, windowHeight * 0.815)
            ctx.setFillStyle('#888888')
            ctx.setFontSize(12)
            ctx.fillText('和我们一起', (windowWidth - 50) * 1 / 2 + 83, windowHeight * 0.815)

            ctx.setFillStyle('#888888')
            ctx.setFontSize(12)
            ctx.setTextAlign('left')
            ctx.fillText('节约用纸,珍惜资源吧！', (windowWidth - 50) * 1 / 2, windowHeight * 0.845)

            ctx.drawImage('../../img/xcx.png', 110, windowHeight * 0.9, 16, 16);
            ctx.setFillStyle('#666666')
            ctx.setFontSize(12)
            ctx.fillText('由小程序\"赢火虫名片\"生成', 130, windowHeight * 0.92)
            ctx.draw()

            setTimeout(function () {
              wx.canvasToTempFilePath({
                x: 0,
                y: 0,
                // width: windowWidth,
                // height: windowHeight,
                // destWidth: windowWidth,
                // destHeight: windowHeight,
                quality: 1,
                canvasId: 'myQrCodeCanvas',
                success: function (res) {
                  console.log("导出成功临时路径tempFilePath=", res.tempFilePath)
                  wx.previewImage({
                    current: res.tempFilePath, // 当前显示图片的http链接
                    urls: [res.tempFilePath] // 需要预览的图片http链接列表
                  });
                  wx.hideLoading();
                },
                fail: function (res) {
                  console.log("导出失败临时路径tempFilePath=", res.tempFilePath)
                }
              })
            }, 100);
          },
          fail: function (res) {
            console.log("下载失败=", res)
          }
        })
      },
      fail: function (res) {
        console.log("下载失败=", res)
      }
    })

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    wx.stopPullDownRefresh();
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  }
})
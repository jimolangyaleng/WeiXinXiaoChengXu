// pages/liuyan/liuyan.js
import formatDate from '../../utils/util.js';
import request from '../../utils/config.js';
const app = getApp();
Page({
  /**
   * 页面的初始数据
   */
  data: {
    ossImgUrL: 'https://winhc.oss-cn-shanghai.aliyuncs.com/xcx/',
    hasMessage: true,
    selectShow: true,
    whoLiuyanTome:true,
    userName:"",
    inputMsg:"",
    leaveMessageList: new Array()
  },

  //谁给我留言
  whoLiuyan: function () {
    this.setData({
      selectShow: true
    })
    wx.showLoading({
      title: '加载中',
    })
    let _that = this;
    //查询留言列表
    let bodyInfo = {
      pageNum: "1",
      pageSize: "100",
      openid: app.globalData.openid
    };
    request.get_api("myReceMessages", bodyInfo, (data) => {
      console.log("查询留言列表成功", data)
      wx.hideLoading()
      let messageList = JSON.parse(data.data.body);
      if (messageList.length > 0) {
        messageList.forEach(function (e) {
          e.userInfo.time = formatDate.formatDate(e.userInfo.time);
          e.showHuiFuMsg = false;
          if (e.replayMessage){
            e.showHuiFuMsg = true;
          }
          if (e.replyUserInfo) {
            if (e.replyUserInfo.time != null) {
              e.replyUserInfo.time = formatDate.formatDate(e.replyUserInfo.time);
            }
          }
        });
        _that.setData({
          leaveMessageList: messageList,
          whoLiuyanTome: true,
          hasMessage: false
        });
      }else{
        _that.setData({
          leaveMessageList: new Array(),
          whoLiuyanTome: true,
          hasMessage: true
        });
      }
    }, (data) => {
      wx.hideLoading()
      _that.setData({
        leaveMessageList: new Array(),
        whoLiuyanTome: true,
        hasMessage: true
      });
      console.log("查询留言列表失败", data)
    });
  },

  //我给谁留言
  iLiuyan: function () {
    this.setData({
      selectShow: false
    })
    wx.showLoading({
      title: '加载中',
    })
    let _that = this;
    //查询留言列表
    let bodyInfo = {
      pageNum: "1",
      pageSize: "100",
      openid: app.globalData.openid
    };
    request.get_api("myLeavMessages", bodyInfo, (data) => {
      console.log("查询我给谁流过言列表成功", data)
      wx.hideLoading()
      let messageList = JSON.parse(data.data.body);
      if (messageList.length > 0) {
        messageList.forEach(function (e) {
          e.userInfo.time = formatDate.formatDate(e.userInfo.time);
          e.showHuiFuMsg = false;
          if (e.replyUserInfo) {
            if (e.replyUserInfo.time!= null) {
              e.replyUserInfo.time = formatDate.formatDate(e.replyUserInfo.time);
            }
          }
        });
        _that.setData({
          leaveMessageList: messageList,
          whoLiuyanTome: false,
          hasMessage: false
        });
      }else{
        _that.setData({
          leaveMessageList: new Array(),
          whoLiuyanTome: false,
          hasMessage: true
        });
      }
    }, (data) => {
      wx.hideLoading();
      _that.setData({
        leaveMessageList: new Array(),
        whoLiuyanTome: false,
        hasMessage: true
      });
      console.log("查询我给谁留过言列表失败", data)
    });
  },

  //删除留言
  delMessage: function (e) {
     // 0留言1回复
    let messageId = e.target.dataset.id;
    let msgChatType = 0;
    let flag = e.target.dataset.flag;
    let tipMsg = '确定删除该留言？';
    if (flag == 'ly'){
      msgChatType = 0;
    } else if (flag == 'hf'){
      msgChatType = 1;
      tipMsg = '确定删除该回复？';
    }
    let queryType = '1';
    if (!this.data.whoLiuyanTome){
        queryType = '0';
    }
    let bodyInfo = {
      openid: app.globalData.openid,
      otherOpenId: app.globalData.openid,
      msgChatType: msgChatType,
      pageNum: "1",
      pageSize: "100",
      queryType: queryType,
      messageId: messageId
    };
    let _that = this;
    wx.showModal({
      title: '删除提醒',
      content: tipMsg,
      success: function (res) {
        if (res.confirm) {
          wx.showLoading({
            title: '正在删除',
          })
          request.post_api("delMessages", bodyInfo, (data) => {
            console.log("删除留言成功", data)
            wx.hideLoading();
            wx.showToast({
              title: '删除成功',
              icon: 'success',
              duration: 1000
            })
            let messageList = JSON.parse(data.data.body);
            if (messageList.length > 0) {
              messageList.forEach(function (e) {
                e.userInfo.time = formatDate.formatDate(e.userInfo.time);
                e.showHuiFuMsg = false;
                if (e.replyUserInfo) {
                  if (e.replyUserInfo.time != null) {
                    e.replyUserInfo.time = formatDate.formatDate(e.replyUserInfo.time);
                  }
                }        
              });
              _that.setData({
                leaveMessageList: messageList,
                hasMessage: false
              });
            } else {
              _that.setData({
                leaveMessageList: messageList,
                hasMessage: true
              });
            }
          }, (data) => {
            console.log("删除留言失败", data)
          });
        } else if (res.cancel) {
          //不做处理
        }
      }
    })
  },

  //回复留言
  huifuAction: function(e){
    let messageId = e.currentTarget.dataset.id;
    let index = e.currentTarget.dataset.key;
    let tempArray = this.data.leaveMessageList;
    tempArray[index].showHuiFuMsg = true;
    this.setData({
      leaveMessageList: tempArray
    });
  },

  //保存留言
  msgInputValue: function(e){
    let value = e.detail.value;
    this.setData({
      inputMsg: value
    });
  },
  //点击进入浏览用户信息页面
  goSeeOtherIndex: function (e) {
    let openid = e.currentTarget.dataset.openid;
    if (openid){
      wx.navigateTo({
        url: '../otherIndex/otherIndex?openid=' + openid
      })
    }
  },

  //input失焦
  blurInput: function(e){
    let value = e.detail.value;
    let messageId = e.currentTarget.dataset.id;
    let index = e.currentTarget.dataset.key;
    let otherOpenid = e.currentTarget.dataset.openid;
    let tempArray = this.data.leaveMessageList;
    tempArray[index].showHuiFuMsg = false;
    if (!value){
      this.setData({
        leaveMessageList: tempArray
      });
      return;
    }
    
    //回复留言
    let _that = this;
    let bodyInfo = {
      openid: app.globalData.openid,
      otherOpenId: app.globalData.openid,
      replyMsg: value,
      messageId: messageId,
      pageNum: "1",
      pageSize: "100",
      msgChatType: 1
    };
    request.post_api("leavdMessages", bodyInfo, (data) => {
      console.log("回复留言成功", data)
      let bodyInfo = JSON.parse(data.data.body);
      bodyInfo.forEach(function (e) {
        e.userInfo.time = formatDate.formatDate(e.userInfo.time);
        
        e.showHuiFuMsg = false;
        if (e.replayMessage){
          e.showHuiFuMsg = true;
        }
        if (e.replyUserInfo) {
          if (e.replyUserInfo.time != null) {
            e.replyUserInfo.time = formatDate.formatDate(e.replyUserInfo.time);
          }
        }
      });
      _that.setData({
        leaveMessageList: bodyInfo
      });
    }, (data) => {
      console.log("回复留言失败", data)
    });
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
      userName: app.globalData.persionalInfo.userName
    });
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.whoLiuyan();
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    if (this.data.selectShow) {
      wx.showLoading({
        title: '加载中',
      })
      let _that = this;
      //查询留言列表
      let bodyInfo = {
        pageNum: "1",
        pageSize: "100",
        openid: app.globalData.openid
      };
      request.get_api("myReceMessages", bodyInfo, (data) => {
        console.log("查询留言列表成功", data)
        wx.hideLoading()
        let messageList = JSON.parse(data.data.body);
        if (messageList.length > 0) {
          messageList.forEach(function (e) {
            e.userInfo.time = formatDate.formatDate(e.userInfo.time);
            e.showHuiFuMsg = false;
            if (e.replyUserInfo) {
              if (e.replyUserInfo.time != null) {
                e.replyUserInfo.time = formatDate.formatDate(e.replyUserInfo.time);
              }
            }
          });
          _that.setData({
            leaveMessageList: messageList,
            whoLiuyanTome: true,
            hasMessage: false
          });
        } else {
          _that.setData({
            leaveMessageList: new Array(),
            whoLiuyanTome: true,
            hasMessage: true
          });
        }
        wx.stopPullDownRefresh();
      }, (data) => {
        wx.hideLoading()
        _that.setData({
          leaveMessageList: new Array(),
          whoLiuyanTome: true,
          hasMessage: true
        });
        console.log("查询留言列表失败", data)
      });
    } else {
      wx.showLoading({
        title: '加载中',
      })
      let _that = this;
      //查询留言列表
      let bodyInfo = {
        pageNum: "1",
        pageSize: "100",
        openid: app.globalData.openid
      };
      request.get_api("myLeavMessages", bodyInfo, (data) => {
        console.log("查询我给谁流过言列表成功", data)
        wx.hideLoading()
        let messageList = JSON.parse(data.data.body);
        if (messageList.length > 0) {
          messageList.forEach(function (e) {
            e.userInfo.time = formatDate.formatDate(e.userInfo.time);
            e.showHuiFuMsg = false;
            if (e.replyUserInfo) {
              if (e.replyUserInfo.time != null) {
                e.replyUserInfo.time = formatDate.formatDate(e.replyUserInfo.time);
              }
            }
          });
          _that.setData({
            leaveMessageList: messageList,
            whoLiuyanTome: false,
            hasMessage: false
          });
        } else {
          _that.setData({
            leaveMessageList: new Array(),
            whoLiuyanTome: false,
            hasMessage: true
          });
        }
        wx.stopPullDownRefresh();
      }, (data) => {
        wx.hideLoading();
        _that.setData({
          leaveMessageList: new Array(),
          whoLiuyanTome: false,
          hasMessage: true
        });
        console.log("查询我给谁留过言列表失败", data)
      });
    }
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})
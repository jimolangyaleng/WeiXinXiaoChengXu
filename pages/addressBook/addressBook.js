import hanziToPinyin from "../../utils/hanziToPinyin.js";
import formatDate from '../../utils/util.js';
import request from '../../utils/config.js';
const app = getApp();
Page({
  /**
   * 页面的初始数据
   */
  data: {
    ossImgUrL: 'https://winhc.oss-cn-shanghai.aliyuncs.com/xcx/',
    topMain: true,     // 搜索条件
    Search: false,     // 只显示搜索框
    searchMain: true,  //显示列表内容
    showKeyDel: "none",
    keyValue: "",
    parentList: new Array(),
    bussinessInfoList: new Array(),
    searchBusInfoList: new Array(),
    selectConditionList:[
      { id: 1, name: "按时间", img: "timeSort.png", img_slt: "timeSort_slt.png", slt: true },
      { id: 2, name: "按姓名",img: "nameSort.png", img_slt:"nameSort_slt.png",slt:false},
      { id: 3, name: "按公司", img: "companySort.png", img_slt: "companySort_slt.png", slt: false }
    ],
    tipmsg: '按姓名',
    tipmsgImg: 'nameSort.png',
    chooseCondition: false,
    isRegister: 'none',
    notRegister: 'none'
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    
  },

  // 判断是否注册名片
  getLoginInfo: (_that) => {
    //获取第三方接口登录数据
    let bodyInfo = {
      openid: app.globalData.openid
    };
    request.get_api("login", bodyInfo, (res) => {
      wx.hideLoading();
      console.log("获取信息成功=", res)
      let bodyData = JSON.parse(res.data.body);
      if (res.data.isSuccess == 'T' && bodyData.memUserInfo.mobileNo) {
        _that.setData({
          isRegister: 'block',
          notRegister: 'none'
        });
      } else {
        console.log('未注册啊');
        _that.setData({
          isRegister: 'none',
          notRegister: 'block'
        });
      }
    }, (res) => {
      // wx.hideLoading();
      console.log("获取信息失败=", res)
    });
  },
  

  // 按时间 姓名 公司
  tapMainMenu: function (e) {     // 获取当前显示的一级菜单标识
    this.setData({
      searchMain: true,
      chooseCondition: !this.data.chooseCondition
    })
  },
  // 搜索
  tapSearch: function() {
    this.setData({
      topMain: false,
      Search: true,   
      searchMain: false,
      chooseCondition: false
    })
  },
  // 取消搜索
  hideTopSearch: function() {
    this.setData({
      Search: false,
      topMain: true,
      searchMain: true
    })
  },

  //搜索框聚焦
  keyFocus: function(){
    this.setData({
      showKeyDel: "block"
    });
  },

  //搜索框失焦
  keyBlur: function(){
    this.setData({
      showKeyDel: "none"
    });
  },

  //搜索框触发事件
  changeKey: function(e){
    var value = e.detail.value;
    this.setData({
      keyValue: value
    });
    //开始出发搜索条件
    var tempList = new Array();
    var originalList = this.data.parentList;
    if (value){
      for (var item in originalList) {
        if (originalList[item].userName){
          if (originalList[item].userName.indexOf(value) != -1) {
            tempList.push(originalList[item]);
          }
        }
      }
    }
    
    this.setData({
      searchBusInfoList: tempList
    });
  },

  //清除搜索框事件
  delKeyTap: function(){
    this.setData({
      keyValue: "",
      searchBusInfoList: new Array()
    });
  },

  // 搜索条件  姓名 时间 公司
  chooseCondition: function(e){
    var tempList = this.data.selectConditionList;
    var index = e.currentTarget.dataset.key;
    tempList.forEach(function(e){
      e.slt = false;
    });
    tempList[index].slt = true;
    //开始运用过滤条件
    if (tempList[index].name === '按姓名'){
      this.sortByname();
    } else if (tempList[index].name === '按时间'){
      this.sortByTime(this.data.parentList,this);
    } else if (tempList[index].name === '按公司'){
      this.sortByCompany();
    }

    this.setData({
      chooseCondition: false,
      selectConditionList: tempList,
      tipmsg: tempList[index].name,
      tipmsgImg: tempList[index].img
    });
  },

  

  //按姓名进行排序
  sortByname: function(){
    var tempList = this.data.parentList;
    for (var item in tempList) {
      var pin = hanziToPinyin(tempList[item].userName.substring(0, 1));
      var code = pin.substring(0, 1);
      tempList[item].code = code;
    }

    var searchLetter = ["A", "B", "C", "D", "E", "F", "G", "H", "J", "K", "L", "M", "N", "P", "Q", "R", "S", "T", "W", "X", "Y", "Z"];
    //对姓名信息进行分组
    var tempObj = [];
    for (var i = 0; i < searchLetter.length; i++) {
      var initial = searchLetter[i];
      var busInfoList = [];
      var tempArr = {};
      tempArr.initial = initial;
      for (var j = 0; j < tempList.length; j++) {
        if (initial == tempList[j].code) {
          busInfoList.push(tempList[j]);
        }
      }
      if (busInfoList.length > 0) {
        tempArr.busInfoList = busInfoList;
        tempObj.push(tempArr);
      }
    }
    this.setData({
      bussinessInfoList: tempObj
    });
  },

  // 按时间进行排序
  sortByTime: function (tempList, _that) {
    let filterArray = formatDate.sortByTime(tempList);
    filterArray.forEach(function (e) {
      e.initial = formatDate.formatDate(e.initial);
    });
    _that.setData({
      bussinessInfoList: filterArray
    });
  },

  // 按公司名称进行排序
  sortByCompany: function () {
    var tempList = this.data.parentList;
    var map = {},
      dest = [];
    for (var i = 0; i < tempList.length; i++) {
      var ai = tempList[i];
      if (!map[ai.lawfirmName]) {
        dest.push({
          initial: ai.lawfirmName,
          busInfoList: [ai]
        });
        map[ai.lawfirmName] = ai;
      } else {
        for (var j = 0; j < dest.length; j++) {
          var dj = dest[j];
          if (dj.initial == ai.lawfirmName) {
            dj.busInfoList.push(ai);
            break;
          }
        }
      }
    }
    // console.log(dest);
    this.setData({
      bussinessInfoList: dest
    });
  },

  goIndex: function(e){
    // wx.switchTab({
    //   url: '../index/index'
    // })
    let openid = e.currentTarget.dataset.openid;
    wx.navigateTo({
      url: '../otherIndex/otherIndex?openid=' + openid
    })
  },

  goCall: function(e) {
    var iPhone = e.currentTarget.dataset.phone;
    wx.makePhoneCall({
      phoneNumber: iPhone
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.getLoginInfo(this)
    wx.showLoading({
      title: '加载中',
    })
    let bodyInfo = {
      pageNum: "1",
      pageSize: "100",
      openid: app.globalData.openid
    };
    let _that = this;
    request.get_api("userFavorOther", bodyInfo, (data) => {
      console.log("我收藏了谁列表成功", data)
      let bodyInfo = JSON.parse(data.data.body);
      let tempArray = new Array();
      bodyInfo.forEach(function(e){
        if (e.avatarUrl){
          tempArray.push(e);
        }
      });
      _that.setData({
        parentList: tempArray
      });
      _that.sortByTime(tempArray, _that)

      wx.hideLoading()
    }, (data) => {
      wx.hideLoading()
      console.log("我收藏了谁列表失败", data)
    });
    this.getLoginInfo(this);
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
    
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    wx.stopPullDownRefresh()
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    
  }

  /**
   * 用户点击右上角分享
   */
  // onShareAppMessage: function () {
    
  // }
})
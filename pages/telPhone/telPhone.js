import request from '../../utils/config.js';
import hex_md5 from '../../utils/md5.js';
let WXBizDataCrypt = require('../../utils/RdWXBizDataCrypt.js');
const app = getApp()
let startTime = 60;
/* 秒级倒计时 */
function count_down(that) {
  // 渲染倒计时时钟
  that.setData({
    time: startTime
  });

  if (startTime <= 0) {
    that.setData({
      time: 0,
      inputYzmLight: false
    });
    // timeout则跳出递归
    return;
  }
  setTimeout(function () {
    // 放在最后--
    startTime--;
    count_down(that);
  }, 1000)
}
Page({
  data: {
    animationData: {},
    telPhone: '',
    inputYzmLight: true,
    isClickOver: false,
    loading: false,
    returnCheckNo: "",
    VerCode: '',
    hasTel: true,
    time: 0
  },
  onShow: function () {
    
  },
  onLoad: function () {
    
  },

  //获取输入手机号值
  getTelPhone: function (e) {
    var telPhone = e.detail.value;
    this.setData({
      telPhone: e.detail.value
    })
    if (telPhone.length == 11) {
      this.setData({
        inputYzmLight:false
      })
    }
  },

  getVerCode: function (e) {
    this.setData({
      VerCode: e.detail.value
    })
  },

  //点击获取验证码 
  yzTel: function () {
    if (!this.data.telPhone){
      this.showErrorMsg("请输入手机号");
      return;
    }
    this.setData({
      inputYzmLight: true
    });
    count_down(this);
    wx.showLoading({
      title: '加载中',
    });
    let bodyInfo = {
      "mobileNo": this.data.telPhone,
      "smsKind": 'checkNo'
    };
    request.get_api("sendMas", bodyInfo, (res) => {
      wx.hideLoading();
      console.log("获取验证码成功=", res)
      if (res.data.isSuccess == 'T') {
        //保存数据
        let body = JSON.parse(res.data.body);
        this.setData({
          returnCheckNo: body.checkNo
        });
      }
    }, (res) => {
      wx.hideLoading();
      console.log("获取验证码失败=", res)
    });

  },
  // 验证码请求成功  点击保存
  submit: function () {
    
    var VerCode = this.data.VerCode;
    var telPhone = this.data.telPhone;
    if (!telPhone){
      this.showErrorMsg("请输入手机号");
      return
    } else if (!this.data.hasTel){
      if (!VerCode){
        this.showErrorMsg("请输入验证码");
        return
      }
    }
    this.setData({
      loading: true
    });
    if (this.data.hasTel){
      VerCode = "SUCCESS";
    }
    let bodyInfo = {
      "openid": app.globalData.openid,
      "mobileNo": this.data.telPhone,
      "checkNo": VerCode
    };
    let _that = this;
    request.post_api("changeMobile", bodyInfo, (res) => {
      wx.hideLoading();
      _that.setData({
        loading: false
      });
      console.log("修改手机号成功=", res)
      if (res.data.isSuccess == 'T') {
        //保存数据
        let body = JSON.parse(res.data.body);
        //保存数据    
        app.globalData.persionalInfo = body;
        wx.navigateBack();
      }else{
        _that.showErrorMsg(res.data.errorCode);
      }
    }, (res) => {
      _that.setData({
        loading: false
      });
     
      console.log("修改手机号失败=", res)
    });
  },
  //快速获取手机号权限
  getPhoneNumber: function (e) {
    // console.log(e.detail.errMsg)
    // console.log(e.detail.iv)
    // console.log(e.detail.encryptedData)
    let _that = this;
    console.log("getPhoneNumber=", e)
    if (e.detail.errMsg == 'getPhoneNumber:fail user deny' || e.detail.errMsg == 'getPhoneNumber:fail:cancel to confirm login' || e.detail.errMsg == 'getPhoneNumber:fail:access denied') {
      _that.showErrorMsg("快速获取失败，请手动输入");
      _that.setData({
        hasTel:false,
      });
    } else {
      let pc = new WXBizDataCrypt(app.globalData.appid, app.globalData.session_key);
      let decodeData = pc.decryptData(e.detail.encryptedData, e.detail.iv);
      console.log('解密后 decodeData: ', decodeData);
      _that.setData({
        telPhone: decodeData.purePhoneNumber,
        isClickOver: true
      });
    }
  },
  //errorMsg显示
  showErrorMsg: function (msg) {
    this.setData({
      errorMsg: msg
    });
    var animation = wx.createAnimation({
      duration: 1000,
      timingFunction: 'ease',
    })
    this.animation = animation
    animation.translateY(26).step()
    this.setData({
      animationData: animation.export()
    })
    setTimeout(function () {
      animation.translateY(-26).step()
      this.setData({
        animationData: animation.export()
      })
    }.bind(this), 2000)
  },
})
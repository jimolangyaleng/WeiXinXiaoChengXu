// pages/toliuyan/toliuyan.js
import formatDate from '../../utils/util.js';
import request from '../../utils/config.js';
const app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    ossImgUrL: 'https://winhc.oss-cn-shanghai.aliyuncs.com/xcx/',
    hasMessage: true,
    showInput: true,
    leaveMessageList: new Array(),
    openid: app.globalData.openid,
    autoFocus: false,
    message: ""
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
  
  },

  //开始留言
  startLiuyan: function(){
    console.log("开始留言")
    this.setData({
      showInput: false,
      autoFocus: true
    });
  },

  //取消留言
  cancleLiuyan: function(){
    this.setData({
      showInput: true
    });
  },

  //确定留言
  confirmLiuyan: function(){
    // 0留言1回复
    if(!this.data.message){
      return;
    }
    wx.showLoading({
      title: '加载中',
    })
    let otherOpenId = wx.getStorageSync("otherOpenid");
    let _that = this;
    let bodyInfo = {
      openid: app.globalData.openid,
      otherOpenId: otherOpenId,
      message: this.data.message,
      pageNum: "1",
      pageSize: "100",
      msgChatType:0
    };
    request.post_api("leavdMessages", bodyInfo, (data) => {
      console.log("提交留言成功", data)
      wx.hideLoading()
      let messageList = JSON.parse(data.data.body);
      if (messageList.length > 0) {
        //过滤时间
        messageList.forEach(function(e){
          e.userInfo.time = formatDate.formatDate(e.userInfo.time);
          if (e.replyUserInfo) {
            if (e.replyUserInfo.time != null) {
              e.replyUserInfo.time = formatDate.formatDate(e.replyUserInfo.time);
            }
          }     
        });
        
        _that.setData({
          leaveMessageList: messageList,
          showInput: true,
          hasMessage: false
        });
      }
    }, (data) => {
      wx.hideLoading()
      console.log("提交留言失败", data)
    });
  },

  //删除留言
  delMessage: function(e){
    let messageId = e.target.dataset.id;
    let otherOpenId = wx.getStorageSync("otherOpenid");
    let bodyInfo = {
      openid: app.globalData.openid,
      otherOpenId: otherOpenId,
      msgChatType: 0,
      queryType: '1',
      pageNum: "1",
      pageSize: "100",
      messageId: messageId
    };
    let _that = this;
    wx.showModal({
      title: '删除提醒',
      content: '确定删除该留言？',
      success: function (res) {
        if (res.confirm) {
          wx.showLoading({
            title: '正在删除',
          })
          request.post_api("delMessages", bodyInfo, (data) => {
            console.log("删除留言成功", data)
            wx.hideLoading();
            wx.showToast({
              title: '删除成功',
              icon: 'success',
              duration: 1000
            })
            let messageList = JSON.parse(data.data.body);
            if (messageList.length > 0) {
              messageList.forEach(function (e) {
                e.userInfo.time = formatDate.formatDate(e.userInfo.time);
                if (e.replyUserInfo){
                  if (e.replyUserInfo.time != null) {
                    e.replyUserInfo.time = formatDate.formatDate(e.replyUserInfo.time); 
                  }
                }   
              });
              _that.setData({
                leaveMessageList: messageList,
                hasMessage: false
              });
            } else {
              _that.setData({
                leaveMessageList: messageList,
                hasMessage: true
              });
            }
          }, (data) => {
            console.log("删除留言失败", data)
          });
        } else if (res.cancel) {
          //不做处理
        }
      }
    })
  },

  //触发信息
  saveTextArea: function(e){
    let det = e.detail.value;
    this.setData({
      message: det
    });
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    wx.showLoading({
      title: '加载中',
    })
    let _that = this;
    let otherOpenId = wx.getStorageSync("otherOpenid");
    console.log("otherOpenId=" + otherOpenId)
    //查询留言列表
    let bodyInfo = {
      pageNum: "1",
      pageSize: "100",
      openid: otherOpenId
    };
    request.get_api("myReceMessages", bodyInfo, (data) => {
      console.log("查询留言列表成功", data)
      wx.hideLoading()
      let messageList = JSON.parse(data.data.body);
      if (messageList.length >0){
        messageList.forEach(function (e) {
          e.userInfo.time = formatDate.formatDate(e.userInfo.time);
          if (e.replyUserInfo) {
            if (e.replyUserInfo.time != null) {
              e.replyUserInfo.time = formatDate.formatDate(e.replyUserInfo.time);
            }
          }
        });
        _that.setData({
          leaveMessageList: messageList,
          hasMessage: false
        });
      }
    }, (data) => {
      wx.hideLoading()
      console.log("查询留言列表失败", data)
    });
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    wx.showLoading({
      title: '加载中',
    })
    let _that = this;
    let otherOpenId = wx.getStorageSync("otherOpenid");
    console.log("otherOpenId=" + otherOpenId)
    //查询留言列表
    let bodyInfo = {
      pageNum: "1",
      pageSize: "100",
      openid: otherOpenId
    };
    request.get_api("myReceMessages", bodyInfo, (data) => {
      console.log("查询留言列表成功", data)
      wx.hideLoading()
      let messageList = JSON.parse(data.data.body);
      if (messageList.length > 0) {
        messageList.forEach(function (e) {
          e.userInfo.time = formatDate.formatDate(e.userInfo.time);
          if (e.replyUserInfo) {
            if (e.replyUserInfo.time != null) {
              e.replyUserInfo.time = formatDate.formatDate(e.replyUserInfo.time);
            }
          }
        });
        _that.setData({
          leaveMessageList: messageList,
          hasMessage: false
        });
      }
      wx.stopPullDownRefresh();
    }, (data) => {
      wx.hideLoading()
      console.log("查询留言列表失败", data)
    });
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  }
})